import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'navlinks',
  templateUrl: './navlinks.component.html',
  styleUrls: ['./navlinks.component.scss']
})
export class NavlinksComponent implements OnInit {

  navLinks: any[] = [
    {
      "name": "Home",
      "link": ""
    },
    {
      "name": "About Us",
      "link": "about-us"
    },
    {
      "name": "Services",
      "link": "services"
    },
    {
      "name": "Portfolio",
      "link": "portfolio"
    },
    {
      "name": "Our Team",
      "link": ""
    },
    {
      "name": "Contact Us",
      "link": "contact-us"
    }
  ]

  constructor() { }

  ngOnInit() {
  }

}
