import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { HeaderContactUsComponent } from './header-contact-us.component';

describe('HeaderContactUsComponent', () => {
  let component: HeaderContactUsComponent;
  let fixture: ComponentFixture<HeaderContactUsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ HeaderContactUsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(HeaderContactUsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
