import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'header-contact-us',
  templateUrl: './header-contact-us.component.html',
  styleUrls: ['./header-contact-us.component.scss']
})
export class HeaderContactUsComponent implements OnInit {
  headerContactItems: any[] = [
    {
      "imageUrl": "../../../assets/images/icons/email.png",
      "alternatetext": "Email",
      "title": "info@technogency.com"
    },
    {
      "imageUrl": "../../../assets/images/icons/phone.png",
      "alternatetext": "Mobile",
      "title": "+92 300 1234567"
    },
    {
      "imageUrl": "../../../assets/images/icons/phone.png",
      "alternatetext": "Phone",
      "title": "+92 42 31234567"
    },
    {
      "imageUrl": "../../../assets/images/icons/location.png",
      "alternatetext": "Location",
      "title": "Street 101, 39-Nicholson Road, Lahore Pakistan"
    },

  ]
  headerContactSocialItems: any[] = [
    {
      "imageUrl": "../../../assets/images/icons/facebook.png",
      "alternatetext": "Facebook",
      "link": ""
    },
    {
      "imageUrl": "../../../assets/images/icons/twitter.png",
      "alternatetext": "Twitter",
      "link": ""
    },
    {
      "imageUrl": "../../../assets/images/icons/linkedin.png",
      "alternatetext": "linkedIn",
      "link": ""
    }

  ]
  constructor() { }

  ngOnInit() {
  }

}
