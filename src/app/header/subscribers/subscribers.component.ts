import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'subscribers',
  templateUrl: './subscribers.component.html',
  styleUrls: ['./subscribers.component.scss']
})
export class SubscribersComponent implements OnInit {

  title = "Subscribe to Our Newsletter:";
  description = "Become a batter digital marketer.";
  btnValue = "Subscribe";

  constructor() { }

  ngOnInit() {
  }

}
