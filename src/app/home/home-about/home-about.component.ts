import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'home-about',
  templateUrl: './home-about.component.html',
  styleUrls: ['./home-about.component.scss']
})
export class HomeAboutComponent implements OnInit {

  aboutThumbnail = "../../../assets/images/about.png";
  bigLetter = "A";
  headTitle = "About Us";
  description = "We're a group of experienced, enthusiastic, and innovative developers and desginers, specialized in delivering great products to our customers. Our expertise is modern technologies, smart product design, and great user experience.Additionally, we love to create tools and technologies that help developers we believe that collaboration is the key to success.";

  constructor() { }

  ngOnInit() {
  }

}
