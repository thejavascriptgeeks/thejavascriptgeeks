import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'home-banner',
  templateUrl: './home-banner.component.html',
  styleUrls: ['./home-banner.component.scss']
})
export class HomeBannerComponent implements OnInit {

  bannerImage = "../../../assets/images/banner.png";
  bigLetter = "TG";
  headlineTop = "Welcome to";
  headlineTitle = "Technogency";
  headlineBottom = "Crafting the digital future…";
  shortDescription = "Technogency is an information technology, and business solutions company that helps enterprises use innovation and emerging technologies to digitally transform their businesses.";
  constructor() { }

  ngOnInit() {
  }

}
