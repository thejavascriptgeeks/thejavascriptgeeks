import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'home-projects',
  templateUrl: './projects.component.html',
  styleUrls: ['./projects.component.scss']
})
export class ProjectsComponent implements OnInit {

  bigLetter = "O";
  headTitle = "Our Innovations";
  description = "Technogency maintains its position as a leader in providing innovative solutions to the global asset finance and leasing industry. ";

  projectsItems: any[] = [
    {
      "projectThumbnail": "../../../assets/images/projects.png",
      "projectTitle": "Karadoz",
      "projectDescription": "Somewhere in between the Swiss watch and poetry,vocation which generates quality  unique web platforms.",
      "seeDetails": "Explore It",
      "seeDetailsLink": "portfolio"
    },
    {
      "projectThumbnail": "../../../assets/images/projects.png",
      "projectTitle": "Karadoz",
      "projectDescription": "Somewhere in between the Swiss watch and poetry,vocation which generates quality  unique web platforms.",
      "seeDetails": "Explore It",
      "seeDetailsLink": "portfolio"
    }
  ]
  constructor() { }

  ngOnInit() {
  }

}
