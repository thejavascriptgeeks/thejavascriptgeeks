import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'mission-vision',
  templateUrl: './mission-vision.component.html',
  styleUrls: ['./mission-vision.component.scss']
})
export class MissionVisionComponent implements OnInit {
  visionThumbnail = "../../../assets/images/vision.png";
  bigLetter = "V";
  headLine = "Our Vision";
  description = "A professionally designed website will help you emphasize your brand and increase business success! Web design agency PopArt Studio is a professional web design firm that covers all aspects of web design, including graphic design, programming, usability, SEO, Google AdWords, etc. We do not just develop “the looks,” we take care of everything website-related for our clients.";

  constructor() { }

  ngOnInit() {
  }

}
