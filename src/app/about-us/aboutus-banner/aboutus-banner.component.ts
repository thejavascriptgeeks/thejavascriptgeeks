import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'aboutus-banner',
  templateUrl: './aboutus-banner.component.html',
  styleUrls: ['./aboutus-banner.component.scss']
})
export class AboutusBannerComponent implements OnInit {

  bannerImage = "../../../assets/images/about-banner.png";
  bigLetter = "S";
  headlineTop = "Our Story";
  shortDescription = "Technology is changing the way consumers are living their everyday lives and only those brands and companies will survive the challenge that accept this digital transformation and are acting fast to embrace the future.";
  constructor() { }

  ngOnInit() {
  }

}
