import { PortfolioList } from './portfolio-list';

export const PortfolioListItems: PortfolioList[] = [
  {
    id: 1001,
    bigLetter: 'H',
    headLine: 'Hajz Tabib ',
    projectStatus: 'Inprogress',
    description: 'We are Java Script Geeks Studio, а web design agency with fresh, new ideas, and we put an emphasis on genuine and usable experiences.',
    features: [
      {
        "listItem": "web design agency with fresh, new ideas"
      },
      {
        "listItem": "web design agency with fresh"
      },
      {
        "listItem": "web design agency with fresh, new ideas"
      },
      {
        "listItem": "web design agency with fresh"
      }

    ],
    projectLink: 'https://facebook.com',
    projectThumbnail: '../assets/images/projects/hajz-tabib.png'
   },
   {
    id: 1002,
    bigLetter: 'P',
    headLine: 'PCN',
    projectStatus: 'Inprogress',
    description: 'We are Java Script Geeks Studio, а web design agency with fresh, new ideas, and we put an emphasis on genuine and usable experiences.',
    features: [
      {
        "listItem": "web design agency with fresh, new ideas"
      },
      {
        "listItem": "web design agency with fresh"
      },
      {
        "listItem": "web design agency with fresh, new ideas"
      },
      {
        "listItem": "web design agency with fresh"
      }

    ],
    projectLink: 'https://facebook.com',
    projectThumbnail: '../assets/images/projects/pcn.png'
   },
   {
    id: 1003,
    bigLetter: 'S',
    headLine: 'PCN Social Media',
    projectStatus: 'Inprogress',
    description: 'We are Java Script Geeks Studio, а web design agency with fresh, new ideas, and we put an emphasis on genuine and usable experiences.',
    features: [
      {
        "listItem": "web design agency with fresh, new ideas"
      },
      {
        "listItem": "web design agency with fresh"
      },
      {
        "listItem": "web design agency with fresh, new ideas"
      },
      {
        "listItem": "web design agency with fresh"
      }

    ],
    projectLink: 'https://facebook.com',
    projectThumbnail: '../assets/images/projects/pcn-social-media.png'
   },
   {
    id: 1004,
    bigLetter: 'P',
    headLine: 'PCN Nightlife Style ',
    projectStatus: 'Completed',
    description: 'We are Java Script Geeks Studio, а web design agency with fresh, new ideas, and we put an emphasis on genuine and usable experiences.',
    features: [
      {
        "listItem": "web design agency with fresh, new ideas"
      },
      {
        "listItem": "web design agency with fresh"
      },
      {
        "listItem": "web design agency with fresh, new ideas"
      },
      {
        "listItem": "web design agency with fresh"
      }

    ],
    projectLink: 'https://facebook.com',
    projectThumbnail: '../assets/images/projects/pcn-live.png'
   },
   {
    id: 1005,
    bigLetter: 'Y',
    headLine: 'Yogoplay',
    projectStatus: 'Inprogress',
    description: 'We are Java Script Geeks Studio, а web design agency with fresh, new ideas, and we put an emphasis on genuine and usable experiences.',
    features: [
      {
        "listItem": "web design agency with fresh, new ideas"
      },
      {
        "listItem": "web design agency with fresh"
      },
      {
        "listItem": "web design agency with fresh, new ideas"
      },
      {
        "listItem": "web design agency with fresh"
      }

    ],
    projectLink: 'https://facebook.com',
    projectThumbnail: '../assets/images/projects/yogoplay.png'
   }

];
