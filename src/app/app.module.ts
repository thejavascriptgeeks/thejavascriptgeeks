import { BrowserModule } from '@angular/platform-browser';
// import { FormsModule } from '@angular/forms';
import { NgModule } from '@angular/core';
import { ReactiveFormsModule } from '@angular/forms';
//import { SwiperModule } from 'angular2-useful-swiper';

//import { SwiperModule } from 'ngx-useful-swiper';


import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { HeaderComponent } from './header/header.component';
import { FooterComponent } from './footer/footer.component';
import { HomeComponent } from './home/home.component';
import { ServicesComponent } from './services/services.component';
import { HowWorksComponent } from './components/how-works/how-works.component';
import { AboutUsComponent } from './about-us/about-us.component';
import { ProjectsComponent } from './home/projects/projects.component';
import { TestimonialsComponent } from './components/testimonials/testimonials.component';
import { ClientsComponent } from './components/clients/clients.component';
import { FooterTopComponent } from './footer/footer-top/footer-top.component';
import { FooterMainComponent } from './footer/footer-main/footer-main.component';
import { FooterBottomComponent } from './footer/footer-bottom/footer-bottom.component';
import { PortfolioComponent } from './portfolio/portfolio.component';
import { ServicesBannerComponent } from './components/services-banner/services-banner.component';
import { NavlinksComponent } from './header/navlinks/navlinks.component';
import { HeaderContactUsComponent } from './header/header-contact-us/header-contact-us.component';
import { SubscribersComponent } from './header/subscribers/subscribers.component';
import { HomeBannerComponent } from './home/home-banner/home-banner.component';
import { InfoButtonsComponent } from './components/info-buttons/info-buttons.component';
import { HomeAboutComponent } from './home/home-about/home-about.component';
import { NavBarComponent } from './footer/footer-main/nav-bar/nav-bar.component';
import { FooterContactusComponent } from './footer/footer-main/footer-contactus/footer-contactus.component';
import { FooterSocialLinksComponent } from './footer/footer-main/footer-social-links/footer-social-links.component';
import { ContactUsComponent } from './contact-us/contact-us.component';
import { PortfolioDetailsComponent } from './portfolio-details/portfolio-details.component';
import { AboutusBannerComponent } from './about-us/aboutus-banner/aboutus-banner.component';
import { MissionVisionComponent } from './about-us/mission-vision/mission-vision.component';
import { ContactBannerComponent } from './contact-us/contact-banner/contact-banner.component';
import { ContactAddressComponent } from './contact-us/contact-address/contact-address.component';
import { ContactFormComponent } from './contact-us/contact-form/contact-form.component';
import { HomeServicesComponent } from './home/home-services/home-services.component';

@NgModule({
  declarations: [
    AppComponent,
    HeaderComponent,
    FooterComponent,
    HomeComponent,
    ServicesComponent,
    HowWorksComponent,
    AboutUsComponent,
    ProjectsComponent,
    TestimonialsComponent,
    ClientsComponent,
    FooterTopComponent,
    FooterMainComponent,
    FooterBottomComponent,
    PortfolioComponent,
    ServicesBannerComponent,
    NavlinksComponent,
    HeaderContactUsComponent,
    SubscribersComponent,
    HomeBannerComponent,
    InfoButtonsComponent,
    HomeAboutComponent,
    NavBarComponent,
    FooterContactusComponent,
    FooterSocialLinksComponent,
    ContactUsComponent,
    PortfolioDetailsComponent,
    AboutusBannerComponent,
    MissionVisionComponent,
    ContactBannerComponent,
    ContactAddressComponent,
    ContactFormComponent,
    HomeServicesComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    ReactiveFormsModule
    //SwiperModule,
    //Angular2UsefulSwiperModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
