import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'testimonials',
  templateUrl: './testimonials.component.html',
  styleUrls: ['./testimonials.component.scss']
})
export class TestimonialsComponent implements OnInit {

  testimonials: any[] = [
    {
      "bigThumbnail": "../../../assets/images/client.png",
      "bigLetter": "W",
      "headLine": "What Our Clients Say",
      "qoute": "We're a group of experienced, enthusiastic, and innovative developers and designers, specialized in delivering great products to our customers. Our expertise is modern create tools and technologies that help create projects success.",
      "sayBy": "Alezbith"
    },
    {
      "bigThumbnail": "../../../assets/images/client.png",
      "bigLetter": "W",
      "headLine": "What Our Clients Say",
      "qoute": "We're a group of experienced, enthusiastic, and innovative developers and designers, specialized in delivering great products to our customers. Our expertise is modern create tools and technologies that help create projects success.",
      "sayBy": "Alezbith"
    },
    {
      "bigThumbnail": "../../../assets/images/client.png",
      "bigLetter": "W",
      "headLine": "What Our Clients Say",
      "qoute": "We're a group of experienced, enthusiastic, and innovative developers and designers, specialized in delivering great products to our customers. Our expertise is modern create tools and technologies that help create projects success.",
      "sayBy": "Alezbith"
    },
  ]

  constructor() { }

  ngOnInit() {
  }

}
