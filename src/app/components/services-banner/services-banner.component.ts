import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'services-banner',
  templateUrl: './services-banner.component.html',
  styleUrls: ['./services-banner.component.scss']
})
export class ServicesBannerComponent implements OnInit {
  servicesItems: any[] = [
    {
      "id": "webDesign",
      "thumbnail": "../../../assets/images/services/designing.png",
      "bigLetter": "W",
      "headLine": "Web Design",
      "description": "Our experienced web designers ensure that the creative input is replicated in the final visuals. Talented visualizers at Technogency have helped global brands etch their mark in their target markets. To the digital mix we add the right colors from our ingenious palette of creativity and churn out the best integrated campaigns."
    },{
      "id": "webDevelopment",
      "thumbnail": "../../../assets/images/services/development.png",
      "bigLetter": "W",
      "headLine": "Web Development",
      "description": "Custom websites that completely coincide with your business needs and carry our mark of excellence. The advantage of custom web applications is that they are tailored exactly to the way your business works."
    },{
      "id": "graphicDesign",
      "thumbnail": "../../../assets/images/services/graphic.png",
      "bigLetter": "G",
      "headLine": "Graphic Design",
      "description": "The interactive agency is a full-service, interactive agency offering innovative Unique graphic design solutions made with love and in accordance with the newest technologies. design & branding solutions which move the boundaries."
    },{
      "id": "seo",
      "thumbnail": "../../../assets/images/services/seo.png",
      "bigLetter": "S",
      "headLine": "Search Engine Optimization",
      "description": "We have one goal in mind and that is to increase the quality and quantity of traffic to your website through organic search results. We tell you what works and what doesn’t when it comes to having your brand come to the top."
    }
  ]
  constructor() { }

  ngOnInit() {
  }

}
