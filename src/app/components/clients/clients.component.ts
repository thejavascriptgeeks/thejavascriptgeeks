import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'clients',
  templateUrl: './clients.component.html',
  styleUrls: ['./clients.component.scss']
})
export class ClientsComponent implements OnInit {

  clientsLogos: any[] = [
    {
      "clientsLogoIcon": "../../../assets/images/client-1.png"
    },
    {
      "clientsLogoIcon": "../../../assets/images/client-2.png"
    },
    {
      "clientsLogoIcon": "../../../assets/images/client-3.png"
    },
    {
      "clientsLogoIcon": "../../../assets/images/client-4.png"
    },
    {
      "clientsLogoIcon": "../../../assets/images/client-5.png"
    }

  ]

  constructor() { }

  ngOnInit() {
  }

}
