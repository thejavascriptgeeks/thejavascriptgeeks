import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'how-works',
  templateUrl: './how-works.component.html',
  styleUrls: ['./how-works.component.scss']
})
export class HowWorksComponent implements OnInit {

  bigLetter = "H";
  headLine = "How Do We Do It";
  description = "Working at an intersection of technology and media, our expertise lie in the areas of Digital Strategy, Creative, Search Engines, Media, PR & Technology and beyond our services wing, there is an in-house venture arm that works on cutting edge solutions to fuel our own product initiatives.";
  howWorksItems: any[] = [
    {
      "id": "01",
      "title": "Research",
      "description": "We listen and research regarding what you need and how you work."
    },
    {
      "id": "02",
      "title": "Idealation",
      "description": "Idealation and collaboration is the key to our constant growth."
    },
    {
      "id": "03",
      "title": "Design",
      "description": "We design wireframe and mockups based on your needs."
    },
    {
      "id": "04",
      "title": "Development",
      "description": "Create a handcoded prototype of the design to be approved by you."
    },
    {
      "id": "05",
      "title": "Testing",
      "description": "We don't work hard, we work smart to be more productive."
    },
    {
      "id": "06",
      "title": "Launch",
      "description": "Setup deploy the project onto the server and your project is good to go."
    }
  ]

  constructor() { }

  ngOnInit() {
  }

}
