import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'info-buttons',
  templateUrl: './info-buttons.component.html',
  styleUrls: ['./info-buttons.component.scss']
})
export class InfoButtonsComponent implements OnInit {
  infolinks: any[] = [
    {
      "name": "Portfolio",
      "link": "portfolio"
    },
    {
      "name": "Order",
      "link": "contact-us"
    }
  ]
  constructor() { }

  ngOnInit() {
  }


}
