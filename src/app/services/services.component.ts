import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'services',
  templateUrl: './services.component.html',
  styleUrls: ['./services.component.scss']
})
export class ServicesComponent implements OnInit {

  servicesItems: any[] = [
    {
      "bigLetter": "U",
      "headLine": "UI / UX Design",
      "description": "Your website is your digital gateway. Attractive interface, smooth experience and visually appealing designs are of the most important aspect for any website. We are an innovative UI / UX design/development company that specializes in creating user-centred, ascetically pleasing and high-functional website designs that help brands optimize customer experience.",
      "serviceIcon": "../../assets/images/services/graphic.png"
    },
    {
      "bigLetter": "W",
      "headLine": "Web Development",
      "description": "When it comes to web design services, we offer the best. We build things that others deem impossible, that too with great panache. Our dynamic and progressive web development guru’s design skilfully crafted and expertly executed solutions for you after meticulous research and analysis. As a leading web development company, we guide you through the digital landscape and bring phenomenal experiences for our clients.",
      "serviceIcon": "../../assets/images/services/development.png"

    },
    {
      "bigLetter": "M",
      "headLine": "Mobile App Development",
      "description": "We have the expertise so you don’t have to pull your hair. Help us transform your great ideas into great apps. At Technogency, we have the right expertise and experience you need to convert your vision into reality. We don’t just build the app, we make sure it succeeds and acts as a seamless user experience. From the ideation phase to execution, we offer compete mobile application development services to our valued clients.",
      "serviceIcon": "../../assets/images/services/mobileapp.png"

    },
    {
      "bigLetter": "B",
      "headLine": "Branding Design",
      "description": "More than just a fancy term, branding is a critical aspect of marketing. Designing a good logo is critical as it helps customers recognize the brand and establish brand identity, leading to a successful brand. Being a good logo design company we helps our clients design memorable, timeless and relevant logos that leave the right impact on the desired target audience.",
      "serviceIcon": "../../assets/images/services/branding.png"
    },
    {
      "bigLetter": "S",
      "headLine": "Search Engine Optimization",
      "description": "A killer content strategy for SEO is crucial to get the top spots in search engine rankings. Our dedicated team of experienced SEO professionals drive your website to number 1 spot on Google with keyword rich content to get the influx of right visitors on your website. We make sure your business gets found online by taking advantage of our premium SEO services. Do yourself a favour by making the smart decision of choosing us for result-oriented SEO services.",
      "serviceIcon": "../../assets/images/services/seo.png"

    },
    {
      "bigLetter": "S",
      "headLine": "Software Development",
      "description": "Our custom software development services are designed to address the precise needs of our clients. At Technogency we make it our priority to produce top-notch work with flawless user experience and smooth software functionality. Our experts architect develop software and applications for even the most complex business ideas. Producing powerful and effective software’s that are uniquely designed to meet your specifications is what we specialize in.",
      "serviceIcon": "../../assets/images/services/software.png"
    },
    {
      "bigLetter": "C",
      "headLine": "Content Marketing",
      "description": "Content creation is the lifeblood of any digital venture. Make content creation easy and attractive with Technogency’s creatively gifted ingenious team. Captivate your audience with some of the most eye-catching and delightfully original content that stands out along with some creative content marketing solutions. On an otherwise dull page, our skilful writers and artists create relevant content that engages, entertains and informs.",
      "serviceIcon": "../../assets/images/services/content.png"
    }

  ]

  constructor() { }

  ngOnInit() {
  }

}
