export class PortfolioList {
  id: number;
  bigLetter: string;
  headLine: string;
  description: string;
  features: any;
  projectLink: string;
  projectStatus: string;
  projectThumbnail: string;
  // design: any;
  // development: any;
  // feedbacks: string;
  // relatedProjects: any;

}
