import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-portfolio-details',
  templateUrl: './portfolio-details.component.html',
  styleUrls: ['./portfolio-details.component.scss']
})
export class PortfolioDetailsComponent implements OnInit {
  bigLetter = "F";
  headLine = "Features";
  description = "The interactive agency is a full-service, interactive agency offering innovative web Unique web design solutions move the boundaries";
  featuresItems: any[] = [
    {
      "id": "01",
      "title": "Online Shopping",
      "description": "We design wireframe and mockups based on your needs."
    },
    {
      "id": "02",
      "title": "Favorite Deals",
      "description": "We design wireframe and mockups based on your needs."
    },
    {
      "id": "03",
      "title": "Local Offers",
      "description": "We design wireframe and mockups based on your needs."
    },
    {
      "id": "04",
      "title": "Special Offers",
      "description": "We design wireframe and mockups based on your needs."
    },
    {
      "id": "05",
      "title": "Store Finder",
      "description": "We design wireframe and mockups based on your needs."
    },
    {
      "id": "06",
      "title": "Delivery",
      "description": "We design wireframe and mockups based on your needs."
    }
  ]

  bigLetter1 = "D";
  headLine1 = "Design";
  description1 = "The interactive agency is a full-service, interactive agency offering innovative web Unique web design solutions move the boundaries";
  designItems: any[] = [
    {
      "id": "01",
      "title": "Sketch",
      "description": "We design wireframe and mockups based on your needs."
    },
    {
      "id": "02",
      "title": "Adobe Illustrator",
      "description": "We design wireframe and mockups based on your needs."
    },
    {
      "id": "03",
      "title": "Invision",
      "description": "We design wireframe and mockups based on your needs."
    }
  ]
  bigLetter2 = "D";
  headLine2 = "Development";
  description2 = "The interactive agency is a full-service, interactive agency offering innovative web Unique web design solutions move the boundaries";
  developmentItems: any[] = [
    {
      "id": "01",
      "title": "AngularJS",
      "description": "We design wireframe and mockups based on your needs."
    },
    {
      "id": "02",
      "title": "Node JS",
      "description": "We design wireframe and mockups based on your needs."
    },
    {
      "id": "03",
      "title": "Mongo DB",
      "description": "We design wireframe and mockups based on your needs."
    }
  ]
  constructor() { }

  ngOnInit() {
  }

}
