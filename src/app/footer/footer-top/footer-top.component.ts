import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'footer-top',
  templateUrl: './footer-top.component.html',
  styleUrls: ['./footer-top.component.scss']
})
export class FooterTopComponent implements OnInit {

  title = "Do you have any Idea?";
  description = "We distinguish ourselves with a specialized set of skills. Curious about what we can offer you?";
  letsTalkUrl = "contact-us";
  letsTalkBtn = "Let's Talk";

  constructor() { }

  ngOnInit() {
  }

}
