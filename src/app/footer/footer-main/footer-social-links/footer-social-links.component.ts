import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'footer-social-links',
  templateUrl: './footer-social-links.component.html',
  styleUrls: ['./footer-social-links.component.scss']
})
export class FooterSocialLinksComponent implements OnInit {

  footerSocialItems: any[] = [
    {
      "imageUrl": "../../../../assets/images/icons/facebook.png",
      "alternatetext": "Facebook",
      "link": ""
    },
    {
      "imageUrl": "../../../../assets/images/icons/linkedin.png",
      "alternatetext": "LinkedIn",
      "link": ""
    },
    {
      "imageUrl": "../../../../assets/images/icons/twitter.png",
      "alternatetext": "Twitter",
      "link": ""
    }
  ]

  constructor() { }

  ngOnInit() {
  }

}
