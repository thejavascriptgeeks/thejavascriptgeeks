import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { FooterContactusComponent } from './footer-contactus.component';

describe('FooterContactusComponent', () => {
  let component: FooterContactusComponent;
  let fixture: ComponentFixture<FooterContactusComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ FooterContactusComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(FooterContactusComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
