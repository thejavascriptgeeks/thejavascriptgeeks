import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'footer-contactus',
  templateUrl: './footer-contactus.component.html',
  styleUrls: ['./footer-contactus.component.scss']
})
export class FooterContactusComponent implements OnInit {

  footerContactItems: any[] = [
    {
      "imageUrl": "../../../../assets/images/icons/email.png",
      "alternateText": "Email",
      "title": "info@technogency.com"
    },
    {
      "imageUrl": "../../../../assets/images/icons/phone.png",
      "alternateText": "Mobile",
      "title": "+92 300 1234567"
    },
    {
      "imageUrl": "../../../../assets/images/icons/location.png",
      "alternateText": "Location",
      "title": "Street 101, 39-Nicholson Road, Lahore Pakistan"
    },

  ]

  constructor() { }

  ngOnInit() {
  }

}
