import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'footer-nav-bar',
  templateUrl: './nav-bar.component.html',
  styleUrls: ['./nav-bar.component.scss']
})
export class NavBarComponent implements OnInit {

  navLinks: any[] = [
    {
      "name": "Home",
      "link": ""
    },
    {
      "name": "About Us",
      "link": "about-us"
    },
    {
      "name": "Services",
      "link": "services"
    },
    {
      "name": "Portfolio",
      "link": "portfolio"
    },
    {
      "name": "Contact Us",
      "link": "contact-us"
    }
  ]

  constructor() { }

  ngOnInit() {
  }

}
