import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'contact-banner',
  templateUrl: './contact-banner.component.html',
  styleUrls: ['./contact-banner.component.scss']
})
export class ContactBannerComponent implements OnInit {

  bannerImage = "../../../assets/images/about-banner.png";
  bigLetter = "C";
  headlineTop = "Contact Us";
  shortDescription = "Technogency has been enabling brands like yours to harness the power of digital media for more than 5 years now. We help you create value for your customers. If you want to hire the best people around, fill out the form below!";
  constructor() { }

  ngOnInit() {
  }

}
