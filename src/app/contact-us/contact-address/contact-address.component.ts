import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'contact-address',
  templateUrl: './contact-address.component.html',
  styleUrls: ['./contact-address.component.scss']
})
export class ContactAddressComponent implements OnInit {

  contactItems: any[] = [
    {
      "itemIcon": "../../../assets/images/icons/location_black.png",
      "detail": "1-B/C Mahmood Ali Kasuri Road, Gulberg 3, Lahore, Pakistan"
    },
    {
      "itemIcon": "../../../assets/images/icons/email_black.png",
      "detail": "info@thejavascriptgeeks.com"
    },
    {
      "itemIcon": "../../../assets/images/icons/phone_black.png",
      "detail": "+1 421234567"
    },
    {
      "itemIcon": "../../../assets/images/icons/phone_black.png",
      "detail": "+92 300 1234567"
    },
  ]

  constructor() { }

  ngOnInit() {
  }

}
